#![allow(unused)]
use cpal;
use cpal::{EventLoop, StreamData, UnknownTypeOutputBuffer};
use rodio;
use rodio::Sink;
use rodio::Source;
use std::fs::File;
use std::io::BufReader;
use std::time::Duration;

fn main() {
    pitch();
}

fn verbose() {
    let event_loop = EventLoop::new();
    let device = cpal::default_output_device().expect("I can't find any output devices.");
    let mut supported_formats_range = device
        .supported_output_formats()
        .expect("error while querying formats");
    let format = supported_formats_range
        .next()
        .expect("no supported format?!")
        .with_max_sample_rate();
    let stream_id = event_loop.build_output_stream(&device, &format).unwrap();
    event_loop.play_stream(stream_id);
    event_loop.run(move |_stream_id, stream_data| {
        // read or write stream data here
        match stream_data {
            StreamData::Output {
                buffer: UnknownTypeOutputBuffer::U16(mut buffer),
            } => {
                for elem in buffer.iter_mut() {
                    *elem = u16::max_value() / 2;
                }
            }
            StreamData::Output {
                buffer: UnknownTypeOutputBuffer::I16(mut buffer),
            } => {
                for elem in buffer.iter_mut() {
                    *elem = 0;
                }
            }
            StreamData::Output {
                buffer: UnknownTypeOutputBuffer::F32(mut buffer),
            } => {
                for elem in buffer.iter_mut() {
                    *elem = 0.0;
                }
            }
            _ => (),
        }
    });
}

fn pitch() {
    let device = rodio::default_output_device().unwrap();
    let sink = Sink::new(&device);

    let source = rodio::source::SineWave::new(220);
    sink.append(source);
}

fn ogg() {
    let device = rodio::default_output_device().unwrap();

    let file = File::open("./resources/chants.ogg").unwrap();
    let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
    let source = source
        .take_duration(Duration::from_secs(5))
        .repeat_infinite();
    rodio::play_raw(&device, source.convert_samples());
}
